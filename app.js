import Keycloak from "keycloak-connect";
import express from "express";
import http from "http";
import bodyParser from "body-parser";
import cors from "cors";
import Routes from "./routes/fetch"

var path = require('path');
var device = require('express-device');


var keycloak = new Keycloak({
});

const app = express();
app.use(cors());
app.use(device.capture());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(keycloak.middleware({
    admin: '/'
}));


app.use("/",keycloak.protect(), Routes);




let server = http.createServer(app);


const PORT = 8090;


server.listen(PORT, () => {
    console.log(`server is started n port ${PORT}`)
})