import express from "express";

const app = express();

let Router = express.Router();

var organization = [
    {
        _id: "1",
        name: "TCS"
    }, {
        _id: "2",
        name: "360"
    }, {
        _id: "3",
        name: "Walgreens"
    }, {
        _id: "4",
        name: "albertos"
    },
];

let idMapping = organization.reduce((prev, curr) => {
    prev[curr._id] = curr;
    return prev;
}, {})


Router.post("/getOrganization", (req, res) => {

    res.json(organization);

})


Router.delete("/modifyOrganization", (req, res) => {

    let {_id} = req.body;
    if (_id) {
        let index = void 0;
        for (let i = 0; i < organization.length; i++) {
            let {_id: organizationId} = organization[i];
            if (organizationId === _id) {
                index = i;
                break;
            }
        }
        if (index !== void 0) {
            organization.splice(index, 1);
            res.json(organization);
            return;
        }
        res.status(500).send({
            message: "record not found",
            status: "error"
        });
        return;


    }

    res.status(500).send({
        error: "_id is required ",
        status: "error"
    });
    return;

})

Router.put("/modifyOrganization", (req, res) => {

    let {_id, name} = req.body;

    let data = idMapping[_id];

    data.name = name;
    res.json(organization);

})

module.exports = Router;